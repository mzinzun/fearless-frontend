import { BrowserRouter as Router, Routes, Route} from 'react-router-dom';
import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import AttendConferenceForm from './AttendConferenceForm';
import ConferenceForm from './ConferenceForm';
import PresentationForm from './PresentationForm';
import MainPage from './MainPage';
import './App.css';

function App(props) {
   if (props.attendees === undefined) {
      return null;
   }
   return (
      <Router>
         <Nav />
         <div className='container'>
            <Routes>
               <Route index element={<MainPage />} />
               <Route exact path="/attendees" element={<AttendeesList attendees={props.attendees}/>} />
               <Route exact path="/locations/new" element={<LocationForm/>} />
               <Route exact path="/conference/new" element={<ConferenceForm/>} />
               <Route exact path="/attendees/new" element={<AttendConferenceForm/>} />
               <Route exact path="/presentation/new" element={<PresentationForm/>} />
            </Routes>
         </div >
      </Router>
   );
}

export default App;
