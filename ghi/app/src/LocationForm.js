import React, { useEffect, useState } from 'react';
// import React from 'react';

function LocationForm(props) {
   const [location, setLocation] = useState('');
   const [states, setStates] = useState([]);
   const emptyFields = { name: '', room_count: 1, city: '', state: '' }
   const [formInputFields, setFormInputFields] = useState(emptyFields);
   const fetchData = async () => {
      const url = 'http://localhost:8000/api/states/';
      const response = await fetch(url);
      if (response.ok) {
         const data = await response.json();
         setStates(data.states);
      }
   }
   useEffect(() => {
      fetchData();
   }, []);
   const handleSubmit = async (event) => {
      event.preventDefault();
      const data = formInputFields;
      const locationUrl = 'http://localhost:8000/api/locations/';
      const fetchConfig = {
         method: "post",
         body: JSON.stringify(data),
         headers: {
            'Content-Type': 'application/json',
         },
      };
      const response = await fetch(locationUrl, fetchConfig);
      if (response.ok) {
         const newLocation = await response.json();
         console.log(newLocation);
      }

   }
   const handleInputChange = (e) => {
      const temp = { ...formInputFields };
      temp[e.target.name] = e.target.value;
      setFormInputFields(temp);
   }
   return (
      <>
         <div className="row">
            <div className="offset-3 col-6">
               <div className="shadow p-4 mt-4">
                  <h1>Create a new location</h1>
                  <form id="create-location-form">
                     <div className="form-floating mb-3">
                        <input className="form-control" placeholder="Name"
                           type="text"
                           id="name"
                           name="name"
                           value={formInputFields.name}
                           onChange={handleInputChange}
                           required
                        />
                        <label htmlFor="name">Name</label>
                     </div>
                     <div className="form-floating mb-3">
                        <input className="form-control" placeholder="Room count"
                           value={formInputFields.room_count}
                           onChange={handleInputChange}
                           type="number"
                           id="room_count"
                           name="room_count"
                           required
                        />
                        <label htmlFor="room_count">Room count</label>
                     </div>
                     <div className="form-floating mb-3">
                        <input className="form-control" placeholder="City"
                           required
                           type="text"
                           id="city"
                           name="city"
                           value={formInputFields.city}
                           onChange={handleInputChange}
                        />
                        <label htmlFor="city">City</label>
                     </div>
                     <div className="mb-3">
                        <select className="form-select" required
                           id="state"
                           name="state"
                           value={formInputFields.state}
                           onChange={handleInputChange} >
                           <option value="">Choose a state</option>
                           {states.map((state, idx) => {
                              return (
                                 <option key={idx} value={state.abbreviation}>{state.name}</option>
                              );
                           })}

                        </select>
                     </div>
                     <button className="btn btn-primary" onClick={handleSubmit}>Create</button>
                  </form>
               </div>
            </div>
         </div>
      </>
   )
}
export default LocationForm;
