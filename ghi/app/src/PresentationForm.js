import React, {useState, useEffect} from 'react';
function PresentationForm (props){
const emptyFields = {
    presenter_name: "",
    presenter_email: "",
    title: "",
    company_name: "",
    synopsis: "",
    conference: ""
}
const [formInputFields, setFormInputFields] = useState(emptyFields);
const [conferences, setConferences] = useState([]);
const fetchData = async () => {
    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
       const data = await response.json();
       setConferences(data.conferences);
       console.log('conferences: ', data.conferences);
    }
 }
useEffect(() =>{
    console.log("PresentationForm component is mounted");
    fetchData()
},[]);
const handleSubmission = async (e)=>{
    e.preventDefault();
    let data = formInputFields;
    const presentationUrl = `http://localhost:8000/api/conferences/${data.conference}/presentations/`
    const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
           'Content-Type': 'application/json',
        },
     };
console.log ('fetch: ',presentationUrl, data)
    const response = await fetch(presentationUrl,fetchConfig);
      if (response.ok) {
         const newLocation = await response.json();
         console.log(newLocation);
         setFormInputFields(emptyFields);
      }

}
function handleFormInput (e)  {
    let temp = {...formInputFields};
    temp[e.target.name] = e.target.value;
    setFormInputFields(temp)
    console.log("my Form Data", formInputFields);

}
return (
    <div className="row">
    <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
            <h1>Create a new presentation</h1>
            <form id="create-presentation-form">
                <div className="form-floating mb-3">
                    <input className="form-control" placeholder="Name"
                    type="text"
                    id="presenter_name"
                    name="presenter_name"
                    value = {formInputFields.presenter_name}
                    onChange = {handleFormInput}
                        required />
                    <label htmlFor="presenter_name">Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input className="form-control" placeholder="email"
                    type="email"
                    id="presenter_email"
                    name="presenter_email"
                    value = {formInputFields.presenter_email}
                    onChange = {handleFormInput}
                    required />
                    <label htmlFor="presenter_email">Presenter email</label>
                </div>
                <div className="form-floating mb-3">
                    <input className="form-control" placeholder="title"
                    type="text"
                    id="title"
                    name="title"
                    value = {formInputFields.title}
                    onChange = {handleFormInput}
                    required />
                    <label htmlFor="title">Title</label>
                </div>
                <div className="form-floating mb-3">
                    <input className="form-control" placeholder="Company name"
                    type="text"
                    id="company_name"
                    name="company_name"
                    value = {formInputFields.company_name}
                    onChange = {handleFormInput}
                    required/>
                    <label htmlFor="company_name">Company name</label>
                </div>
                <div className=" mb-3">
                    <label>Synopsis</label>
                    <textarea className="form-control"
                    cols="25" rows="3"
                    id="synopsis"
                    name="synopsis"
                    value = {formInputFields.synopsis}
                    onChange = {handleFormInput}
                    ></textarea>
                </div>
                <div className="mb-3">
                    <select  className="form-select"
                    id="conference"
                    name="conference"
                    value = {formInputFields.conference}
                    onChange = {handleFormInput}
                    required>
                         <option value="">Choose a conference</option>
                        {conferences.map((conference, idx) => {
                            return (
                               <option key={idx} value={conference.id}>{conference.name}</option>
                            );
                         })}
                    </select>
                </div>
                <button className="btn btn-primary" onClick={handleSubmission}>Create</button>
            </form>
        </div>
    </div>
</div>
);
}

export default PresentationForm;
